<!DOCTYPE html> 
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Register</title>
<body>

<fieldset style='width: 600px; height: 500px; border:#2980b9 solid'>

<?php
    // define variables
    $name = "";
    $gender = "";
    $faculty = "";
    $dob = "";
    $address = "";

    function handler($data) {
        $data = stripslashes($data);
        $data = trim($data);
        return $data;
    }

    function validateDate($date){
        // validate MM/DD/YYYY
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date)) {
            return true;
        } else {
            return false;
        }
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty(handler($_POST["name"]))) {
            echo "<div style='color: red;'>Hãy nhập tên</div>";
        }
        if (empty($_POST["gender"])) {
            echo "<div style='color: red;'>Hãy chọn giới tính</div>";
        }
        if (empty(handler($_POST["faculty"]))) {
            echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
        }
        if (empty(handler($_POST["dob"]))) {
            echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
        }
        if (!validateDate($_POST["dob"])) {
            echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
        }
    }
?>

<form style='margin: 50px 70px 0 50px' method="post">
    <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
        <tr height = '40px'>
            <td width = 40% style = 'background-color: #2980b9; 
            vertical-align: top; text-align: left; padding: 15px 15px'>
                <label style='color: white;'>Họ tên</label>
            </td>
            <td width = 40% >
                <input type='text' name = "name" style = 'line-height: 32px; border-color:#2980b9'>
            </td>
        </tr>
        <tr height = '40px'>
            <td width = 40% style = 'background-color: #2980b9; 
            vertical-align: top; text-align: left; padding: 15px 15px'>
                <label style='color: white;'>Giới tính</label>
            </td>
            <td width = 40% >
            <?php
                $genderArr=array("Nam","Nữ");
                for($x = 0; $x < count($genderArr); $x++){
                    echo"
                        <label class='container'>
                            <input type='radio' value=".$genderArr[$x]." name='gender'>"
                            .$genderArr[$x]. 
                        "</label>";
                }
            ?>  

            </td>
        </tr>
        <tr height = '40px'>
            <td style = 'background-color: #2980b9; vertical-align: top; text-align: left; padding: 15px 15px'>
                <label style='color: white;'>Phân Khoa</label>
            </td>
            <td height = '40px'>
                <select name='faculty' style = 'border-color:#ADD8E6;height: 100%;width: 80%;'>
                    <?php
                        $facultyArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                        foreach($facultyArr as $x=>$x_value){
                            echo"<option>".$x_value."</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        
        <tr height = '40px'>
            <td style = 'background-color: #2980b9; 
            vertical-align: top; text-align: left; padding: 15px 15px'>
                <label style='color: white;'>Ngày sinh</label>
            </td>
            <td height = '40px'>
                <input type='date' name="dob" data-date="" data-date-format="YYYY-MM-DD" style = 'line-height: 32px; border-color:#2980b9'>
            </td>
        </tr>

        <tr height = '40px'>
            <td style = 'background-color: #2980b9; vertical-align: top; text-align: left; padding: 15px 15px'>
                <label style='color: white;'>Địa chỉ</label>
            </td>
            <td height = '40px'>
                <input type='text' name="address" style = 'line-height: 32px; border-color:#2980b9'> 
            </td>
        </tr>

    </table>
    <button style='background-color: #27ae60; border-radius: 10px; 
    width: 35%; height: 39px; border-width: 0; margin: 20px 130px; color: white;'>Đăng Kí</button>
</form>

</fieldset>
</body>
</html>


